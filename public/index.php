<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Carga del archivo autoluad
require '../vendor/autoload.php';

//Carga del archivo de conexion
require '../src/config/db.php';

/*
$app = new \Slim\App;
$app->get('/hello/{name}', function (Request $request, Response $response, array $args) {
    $name = $args['name'];
    $response->getBody()->write("Hello, $name");

    return $response;
});*/

//Creacion de rutas para los clientes
require "../src/rutas/clientes.php";

$app->run();
