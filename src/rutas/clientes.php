<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

//Instancia de Slim
$app = new \Slim\App;
//Token para autenticar
define('AUTORIZATION', '01deacfe804808b5db53edabdf7532562d3e838b');

//Obtener todos los clientes
$app->get('/api/cliente', function(Request $request,Response $response){
    //Consulta de clientes
    $consulta = 'SELECT * FROM clientes';
    try {
        //Instanciacion de las base de datos
        $db = new db();
        $db = $db->conectar();
        $ejecutar = $db->query($consulta);
        $clientes = $ejecutar->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        //Mostrar en JSON
        return $response->withJson($clientes,200);
    } catch (PDOException $e) {
        $error = '{"error":{"text":'.$e->getMessage().'}}';
        return $response->withJson($error,500);
    }
});

//Obtener un cliente por su id
$app->get('/api/cliente/{id}', function(Request $request,Response $response){
    $id = $request->getAttribute('id');
    $consulta = 'SELECT * FROM clientes WHERE id_cliente = '.$id;
    try {
        //Instanciacion de las base de datos
        $db = new db();
        $db = $db->conectar();
        $ejecutar = $db->query($consulta);
        $cliente = $ejecutar->fetchAll(PDO::FETCH_OBJ);
        $db = null;
        //Mostrar en JSON
        return $response->withJson($cliente,200);
    } catch (PDOException $e) {
        $error = '{"error":{"text":'.$e->getMessage().'}}';
        return $response->withJson($error,500);
    }
});


//Agregar un cliente
$app->post('/api/cliente', function(Request $request,Response $response){
    //Se valida que venga el header del token
    if ($request->hasHeader('x-token')) {
        $token = $request->getHeader('x-token')[0];
        //Se valida que el token enviado coincida con el de autorizacion
        if($token == AUTORIZATION){
            $body = $request->getBody();
            //Se verifiva que se hayan enviado datos
            if($body->getContents() != ''){
                $nombre = $request->getParam('nombre');
                $telefono = $request->getParam('telefono');
                $email = $request->getParam('email');
                //Se valida que los datos obligatoris esten presentes
                if(is_null($nombre) || is_null($telefono) || is_null($email)){
                    return $response->withJson(['error' => 'Unprocessable Entity: The request requires [name, email, phone]'],422);
                }else{
                    $apellidos = $request->getParam('apellidos');
                    $direccion = $request->getParam('direccion');
                    $ciudad = $request->getParam('ciudad');
                    $deaprtamento = $request->getParam('deaprtamento');

                    $consulta = "INSERT INTO clientes(nombre,apellidos,telefono,email,direccion,ciudad,deaprtamento) VALUES
                    (:nombre, :apellidos, :telefono, :email, :direccion, :ciudad, :deaprtamento)";
                    try {
                        //Instanciacion de las base de datos
                        $db = new db();
                        $db = $db->conectar();
                        $stmt = $db->prepare($consulta);
                        $stmt->bindParam(':nombre',$nombre,PDO::PARAM_STR);
                        $stmt->bindParam(':apellidos',$apellidos,PDO::PARAM_STR);
                        $stmt->bindParam(':telefono',$telefono,PDO::PARAM_STR);
                        $stmt->bindParam(':email',$email,PDO::PARAM_STR);
                        $stmt->bindParam(':direccion',$direccion,PDO::PARAM_STR);
                        $stmt->bindParam(':ciudad',$ciudad,PDO::PARAM_STR);
                        $stmt->bindParam(':deaprtamento',$deaprtamento,PDO::PARAM_STR);
                        $stmt->execute();
                        $msg = '{"message":"Cliente agregado"}';
                        return $response->withJson($msg,201);
                    } catch (PDOException $e) {
                        $error = '{"Internal Server Error": '.$e->getMessage().'}';
                        return $response->withJson($error,500);
                    }
                }
            }else{
                //No envio datos
                return $response->withJson(['error' => 'Unprocessable Entity: The request requires data.'],422);
            }
        }else{
            //No coincide el token
            return $response->withJson(['error' => 'Unauthorized: The request requires user authentication.'],401);
        }
    }else{
        //No existe el header
        return $response->withJson(['error' => 'Bad Request: The request could not be understood by the server due to malformed syntax.'],400);
    }
});

//Actualizar los datos de un cliente por id
$app->put('/api/cliente/{id}', function(Request $request,Response $response){
    $method = $request->getMethod();
    //Se valida que venga el header del token
    if ($request->hasHeader('x-token')) {
        $token = $request->getHeader('x-token')[0];
        //Se valida que el token enviado coincida con el de autorizacion
        if($token == AUTORIZATION){
            $body = $request->getBody();
            //Se verifiva que se hayan enviado datos
            if($body->getContents() != ''){
                $id = $request->getAttribute('id');
                $nombre = $request->getParam('nombre');
                $apellidos = $request->getParam('apellidos');
                $telefono = $request->getParam('telefono');
                $email = $request->getParam('email');
                $direccion = $request->getParam('direccion');
                $ciudad = $request->getParam('ciudad');
                $deaprtamento = $request->getParam('deaprtamento');
                //print_r($nombre);
                $consulta = "UPDATE clientes
                            SET
                              nombre = :nombre,
                              apellidos = :apellidos,
                              telefono = :telefono,
                              email = :email,
                              direccion = :direccion,
                              ciudad = :ciudad,
                              deaprtamento = :deaprtamento
                            WHERE id_cliente = $id";
                try {
                    //Instanciacion de las base de datos
                    $db = new db();
                    $db = $db->conectar();
                    $stmt = $db->prepare($consulta);
                    $stmt->bindParam(':nombre',$nombre,PDO::PARAM_STR);
                    $stmt->bindParam(':apellidos',$apellidos,PDO::PARAM_STR);
                    $stmt->bindParam(':telefono',$telefono,PDO::PARAM_STR);
                    $stmt->bindParam(':email',$email,PDO::PARAM_STR);
                    $stmt->bindParam(':direccion',$direccion,PDO::PARAM_STR);
                    $stmt->bindParam(':ciudad',$ciudad,PDO::PARAM_STR);
                    $stmt->bindParam(':deaprtamento',$deaprtamento,PDO::PARAM_STR);
                    $stmt->execute();
                    $msg = '{"message":"Cliente actualizado"}';
                    return $response->withJson($msg,201);
                } catch (PDOException $e) {
                    $error = '{"Internal Server Error": '.$e->getMessage().'}';
                    return $response->withJson($error,500);
                }
            }else{
                //No envio datos
                return $response->withJson(['error' => 'Unprocessable Entity: The request requires data.'],422);
            }
        }else{
            //No coincide el token
            return $response->withJson(['error' => 'Unauthorized: The request requires user authentication.'],401);
        }
    }else{
        //No existe el header
        return $response->withJson(['error' => 'Bad Request: The request could not be understood by the server due to malformed syntax.'],400);
    }
});

//Eliminar a un cliente por su id
$app->delete('/api/cliente/{id}', function(Request $request,Response $response){
    //Se valida que venga el header del token
    if ($request->hasHeader('x-token')) {
        $token = $request->getHeader('x-token')[0];
        //Se valida que el token enviado coincida con el de autorizacion
        if($token == AUTORIZATION){
            $id = $request->getAttribute('id');
            $consulta = 'DELETE FROM clientes WHERE id_cliente = '.$id;
            try {
                //Instanciacion de las base de datos
                $db = new db();
                $db = $db->conectar();
                $stmt = $db->prepare($consulta);
                $stmt->execute();
                $db = null;
                //Mostrar mensaje
                $msg = '{"notice":{"text": "Cliente borrado"}}';
                return $response->withJson($msg,200);
            } catch (PDOException $e) {
                $error = '{"Internal Server Error":'.$e->getMessage().'}';
                return $response->withJson($error,500);
            }
        }else{
            //No coincide el token
            return $response->withJson(['error' => 'Unauthorized'],401);
        }
    }else{
        //No existe el header
        return $response->withJson(['error' => 'Bad Request'],400);
    }
});

?>
